﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FMOD_TextTimedSequencerv2Light : MonoBehaviour
{
    public float multiplier;
    //public float stepTime;
    [TextArea]
    public string text;
    public char[] elements;
    public AudioChar[] aChar;
    public FMODAudioReactiveLight[] emitter;
    public bool isRandomSequence;

    public int barMin;
    public int barMax;

    public int length;
    public char[] sequence;
    string stringSequence;
    string st = "abcdefghilmnopqrstuvz"; //"abcdefghijklmnopqrstuvwxyz";

    // Use this for initialization
    void Start()
    {
        if (isRandomSequence)
        {
            RandomSequence();
        }
        else
        {
            elements = text.ToCharArray();
        }

        aChar = new AudioChar[elements.Length / 2];

        for (int i = 0; i < aChar.Length; i++)
        {
            aChar.SetValue(new AudioChar(), i);
        }

        //print(elements.Length);

        for (int i = 0; i < elements.Length; i++)
        {
            switch (elements[i])
            {
                case 'a':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'b':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'c':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'd':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'e':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'f':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'g':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'h':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'i':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'l':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'm':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'n':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'o':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'p':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'q':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'r':
                    aChar[i / 2].character = elements[i];
                    break;
                case 's':
                    aChar[i / 2].character = elements[i];
                    break;
                case 't':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'u':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'v':
                    aChar[i / 2].character = elements[i];
                    break;
                case 'z':
                    aChar[i / 2].character = elements[i];
                    break;
                case '1':
                    aChar[i / 2].time = 1;
                    break;
                case '2':
                    aChar[i / 2].time = 2;
                    break;
                case '3':
                    aChar[i / 2].time = 3;
                    break;
                case '4':
                    aChar[i / 2].time = 4;
                    break;
                case '5':
                    aChar[i / 2].time = 5;
                    break;
                case '6':
                    aChar[i / 2].time = 6;
                    break;
                case '7':
                    aChar[i / 2].time = 7;
                    break;
                case '8':
                    aChar[i / 2].time = 8;
                    break;
                case '9':
                    aChar[i / 2].time = 9;
                    break;
                case '0':
                    aChar[i / 2].time = 0.5f;
                    break;

                default:
                    break;
            }
        }
        StartCoroutine("Step");
    }

    private void OnEnable()
    {
        StartCoroutine("Step");
    }

    private void OnDisable()
    {
        StopCoroutine("Step");
    }


    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator Step()
    {
        foreach (var item in aChar)
        {
            PlayChar(item.character);
            yield return new WaitForSeconds(item.time * multiplier);
            //print(item.character + " " + item.time);
            StopChar(item.character);
        }
    }

    void PlayChar(char character)
    {
        switch (character)
        {
            case 'a': if (emitter[0] != null) emitter[0].enabled = true; break;
            case 'b': if (emitter[1] != null) emitter[1].enabled = true; break;
            case 'c': if (emitter[2] != null) emitter[2].enabled = true; break;
            case 'd': if (emitter[3] != null) emitter[3].enabled = true; break;
            case 'e': if (emitter[4] != null) emitter[4].enabled = true; break;
            case 'f': if (emitter[5] != null) emitter[5].enabled = true; break;
            case 'g': if (emitter[6] != null) emitter[6].enabled = true; break;
            case 'h': if (emitter[7] != null) emitter[7].enabled = true; break;
            case 'i': if (emitter[8] != null) emitter[8].enabled = true; break;
            case 'l': if (emitter[9] != null) emitter[9].enabled = true; break;
            case 'm': if (emitter[10] != null) emitter[10].enabled = true; break;
            case 'n': if (emitter[11] != null) emitter[11].enabled = true; break;
            case 'o': if (emitter[12] != null) emitter[12].enabled = true; break;
            case 'p': if (emitter[13] != null) emitter[13].enabled = true; break;
            case 'q': if (emitter[14] != null) emitter[14].enabled = true; break;
            case 'r': if (emitter[15] != null) emitter[15].enabled = true; break;
            case 's': if (emitter[16] != null) emitter[16].enabled = true; break;
            case 't': if (emitter[17] != null) emitter[17].enabled = true; break;
            case 'u': if (emitter[18] != null) emitter[18].enabled = true; break;
            case 'v': if (emitter[19] != null) emitter[19].enabled = true; break;
            case 'z': if (emitter[20] != null) emitter[20].enabled = true; break;

            default:
                Debug.Log(character + " doesn't exist in this switch statement"); break;
        }
    }

    void StopChar(char character)
    {
        switch (character)
        {
            case 'a': if (emitter[0] != null) emitter[0].enabled = false; break;
            case 'b': if (emitter[1] != null) emitter[1].enabled = false; break;
            case 'c': if (emitter[2] != null) emitter[2].enabled = false; break;
            case 'd': if (emitter[3] != null) emitter[3].enabled = false; break;
            case 'e': if (emitter[4] != null) emitter[4].enabled = false; break;
            case 'f': if (emitter[5] != null) emitter[5].enabled = false; break;
            case 'g': if (emitter[6] != null) emitter[6].enabled = false; break;
            case 'h': if (emitter[7] != null) emitter[7].enabled = false; break;
            case 'i': if (emitter[8] != null) emitter[8].enabled = false; break;
            case 'l': if (emitter[9] != null) emitter[9].enabled = false; break;
            case 'm': if (emitter[10] != null) emitter[10].enabled = false; break;
            case 'n': if (emitter[11] != null) emitter[11].enabled = false; break;
            case 'o': if (emitter[12] != null) emitter[12].enabled = false; break;
            case 'p': if (emitter[13] != null) emitter[13].enabled = false; break;
            case 'q': if (emitter[14] != null) emitter[14].enabled = false; break;
            case 'r': if (emitter[15] != null) emitter[15].enabled = false; break;
            case 's': if (emitter[16] != null) emitter[16].enabled = false; break;
            case 't': if (emitter[17] != null) emitter[17].enabled = false; break;
            case 'u': if (emitter[18] != null) emitter[18].enabled = false; break;
            case 'v': if (emitter[19] != null) emitter[19].enabled = false; break;
            case 'z': if (emitter[20] != null) emitter[20].enabled = false; break;
            default:
                Debug.Log(character + " doesn't exist in this switch statement"); break;
        }
    }

    // Use this for initialization
    void RandomSequence()
    {
        sequence = new char[length];

        if (st.Length >= emitter.Length)
        {
            for (int i = 0; i < length; i++)
            {
                if (i % 2 == 0)
                {
                    sequence[i] = st[Random.Range(0, emitter.Length)];

                }
                else
                {
                    sequence[i] = Random.Range(barMin, barMax).ToString().ToCharArray()[0];
                }
            }
            elements = sequence;
        }
        else
        {
            print("emitter.Length can't be > than st.length'");
        }
    }
}
