﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FMODAudioReactive : MonoBehaviour {
    public FMOD.ChannelGroup group;

    // channel group and dsp
    FMOD.ChannelGroup channelGroup;
    FMOD.DSP channelDSP;

    // outputmeter
    FMOD.DSP_METERING_INFO Outputmeter;

    //AUDIO
    [FMODUnity.EventRef]
    public string footStepSound;
    FMOD.Studio.EventInstance ev;
    FMOD.Studio.ParameterInstance par;

    public Text text;
    public float mult;

    void OnEnable()
    {
        Outputmeter = new FMOD.DSP_METERING_INFO();
        StartCoroutine("test");
    }

    // Use this for initialization
    void Start () {
        Outputmeter = new FMOD.DSP_METERING_INFO();
        StartCoroutine("test");
    }
	
	// Update is called once per frame
	void Update () {
        
        //int pos;
        //ev.getTimelinePosition(out pos);
        //print("pos" + pos);
    }

    IEnumerator test()
    {
        FMOD.Studio.EventInstance ev = FMODUnity.RuntimeManager.CreateInstance(footStepSound);
        ev.start();

        yield return new WaitForSeconds(0.01f);
        while (true)
        {
            ev.getChannelGroup(out channelGroup);
            if (channelGroup != null)
            {
                // getting the head dsp (I think)
                channelGroup.getDSP(0, out channelDSP);

                // turning metering on
                channelDSP.setMeteringEnabled(false, true);

                // this might be where I’m going wrong – I get an error that says Outputmeter will always be null
                channelDSP.getMeteringInfo(null, Outputmeter);

                // getting the RMS level
                print(Outputmeter.numchannels);
                float outputRMS = Outputmeter.peaklevel[0];
                transform.localScale = new Vector3(outputRMS * mult, outputRMS * mult, outputRMS * mult);

                // displaying the value – currently just displays the default placeholder text rather than any number so not working it seems
                text.text = outputRMS.ToString();
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }
}
