﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatSmoothFollow : MonoBehaviour {

    public float interpVelocity;
    public float minDistance;
    public float followDistance;
    public GameObject target;
    public Vector3 offset;
    Vector3 targetPos;
    public float mult;
    public float scrollSensitivity;
    Camera cam;
    public float camInterpolation;
    // Use this for initialization
    void Start()
    {
        targetPos = transform.position;
        //cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target)
        {
            //Debug.Log(Input.GetAxis("Mouse ScrollWheel") + " " + cam.fieldOfView + Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity); 

            //cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, cam.fieldOfView - Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity, camInterpolation * Time.deltaTime);

            Vector3 posNoZ = transform.position;
            posNoZ.z = target.transform.position.z;

            Vector3 targetDirection = (target.transform.position - posNoZ);

            interpVelocity = targetDirection.magnitude * mult;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            transform.position = Vector3.Lerp(transform.position, targetPos + offset, 0.25f);

        }
    }
}
