﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;


public class FMODAudioReactiveLight : MonoBehaviour
{
    public Light TargetLight;

    public FMOD.ChannelGroup group;

    // channel group and dsp
    FMOD.ChannelGroup channelGroup;
    FMOD.DSP channelDSP;

    // outputmeter
    FMOD.DSP_METERING_INFO Outputmeter;

    //AUDIO
    [FMODUnity.EventRef]
    public string FMODEvent;
    FMOD.Studio.EventInstance ev;
    FMOD.Studio.ParameterInstance par;

    public float mult;

    bool hasToStop = false;
    bool isRunning = false;

    TorchScript torchScript;


    void OnEnable()
    {
        Outputmeter = new FMOD.DSP_METERING_INFO();
        torchScript = GetComponent<TorchScript>();
        torchScript.enabled = false;

        if (TargetLight == null && GetComponent<Light>() != null)
        {
            TargetLight = GetComponent<Light>();
        }

        Outputmeter = new FMOD.DSP_METERING_INFO();

        if (!isRunning)
        {
            StartCoroutine("test");
        }
    }

    // Use this for initialization
    void Start()
    {
        torchScript = GetComponent<TorchScript>();
        torchScript.enabled = false;

        if (TargetLight == null && GetComponent<Light>() != null)
        {
            TargetLight = GetComponent<Light>();
        }

        Outputmeter = new FMOD.DSP_METERING_INFO();
        if (!isRunning)
        {
            StartCoroutine("test");
        }
    }

    // Update is called once per frame
    void Update()
    {

        //int pos;
        //ev.getTimelinePosition(out pos);
        //print("pos" + pos);
    }

    IEnumerator test()
    {
        isRunning = true;
        FMOD.Studio.EventInstance ev = FMODUnity.RuntimeManager.CreateInstance(FMODEvent);
        ev.start();

        yield return new WaitForSeconds(0.01f);
        while (true)
        {
            if (hasToStop)
            {
                //print("HASTOSTOP");
                ev.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                //ev.release();
                //ev = null;
                hasToStop = false;
                isRunning = false;

                StopAllCoroutines();
            }

            ev.getChannelGroup(out channelGroup);
            if (channelGroup != null)
            {
                // getting the head dsp (I think)
                channelGroup.getDSP(0, out channelDSP);

                // turning metering on
                channelDSP.setMeteringEnabled(false, true);

                // this might be where I’m going wrong – I get an error that says Outputmeter will always be null
                channelDSP.getMeteringInfo(null, Outputmeter);

                // getting the RMS level
                //print(Outputmeter.numchannels);
                float outputRMS = Outputmeter.peaklevel[0];
                TargetLight.intensity = outputRMS * mult;

                // displaying the value – currently just displays the default placeholder text rather than any number so not working it seems
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    void OnDisable()
    {
        //StopAllCoroutines();
        //ev.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        hasToStop = true;
        torchScript.enabled = true;
    }
}

