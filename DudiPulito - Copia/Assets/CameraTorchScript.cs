﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTorchScript : MonoBehaviour {

    public float speed;
    public Color min;
    public Color max;
    public int maxCount;
    //public float lightMovement;
    public Camera lg;
    int count = 0;
    bool isRising = true;
    // Use this for initialization
    void OnEnable()
    {
        lg = GetComponent<Camera>();
        StartCoroutine("TorchLight");
        count = Random.Range(0, maxCount);
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator TorchLight()
    {
        while (true)
        {
            if (count >= maxCount)
            {
                isRising = false;
            }
            else if (count <= 0)
            {
                isRising = true;
            }

            if (isRising)
            {
                count++;
            }
            else
            {
                count--;
            }


            //Debug.Log((float)count/(float)maxCount);
            //Debug.Log(count + " " + (float)(count/maxCount));
            lg.backgroundColor = Color.Lerp(min, max, ((float)count / (float)maxCount));
            //transform.localPosition = new Vector3(Mathf.Lerp(transform.localPosition.x - lightMovement, transform.localPosition.x + lightMovement, (float)count / (float)maxCount), transform.localPosition.y, transform.localPosition.z);
            yield return new WaitForSeconds(speed);
        }
    }
}
