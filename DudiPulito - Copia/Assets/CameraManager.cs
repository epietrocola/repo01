﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public GameObject firstPos;
    public GameObject secondPos;
    public GameObject thirdPos;
    public GameObject Camera;
    public int length;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)){
            StopCoroutine("Lurp");
            StartCoroutine("Lurp", firstPos.transform);
            Camera.GetComponent<cameraFollowGit>().offset.y = -4f;
            Camera.GetComponent<cameraFollowGit>().followDistance = 1f;
            Camera.GetComponent<cameraFollowGit>().mult = 10f;


        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StopCoroutine("Lurp");
            StartCoroutine("Lurp", secondPos.transform);
            Camera.GetComponent<cameraFollowGit>().offset.y = -6f;
            Camera.GetComponent<cameraFollowGit>().followDistance = 0f;
            Camera.GetComponent<cameraFollowGit>().mult = 30f;
        }

        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StopCoroutine("Lurp");
            StartCoroutine("Lurp", thirdPos.transform);
            Camera.GetComponent<cameraFollowGit>().offset.y = -80f;
            Camera.GetComponent<cameraFollowGit>().followDistance = 50f;
            Camera.GetComponent<cameraFollowGit>().mult = 30f;
        }
    }

    IEnumerator Lurp(Transform transform)
    {
        for (int i = 0; i < length; i++)
        {
            Transform tempTransform = Camera.transform;

            Camera.transform.position = Vector3.Lerp(tempTransform.position, transform.position, (float)i / (float)length);
            Camera.transform.rotation = Quaternion.Lerp(tempTransform.rotation, transform.rotation, (float)i / (float)length);

            yield return new WaitForSeconds(0.01f);

        }
    }

}
