﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSide : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("RandomChangeSide");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator RandomChangeSide() {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 2f));
            GetComponent<ChatSmoothFollow>().offset.x = Random.Range(-1.6f, 1.6f);
            GetComponent<ChatSmoothFollow>().offset.y = Random.Range(-1.6f, -0.3f);

        }
    }

}
