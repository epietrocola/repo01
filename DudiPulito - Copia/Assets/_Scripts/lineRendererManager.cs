﻿using UnityEngine;
using System.Collections;

public class lineRendererManager : MonoBehaviour {

	public Transform[] points;
	LineRenderer lineRenderer;
	// Use this for initialization
	void Start () {
		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetVertexCount(points.Length);
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < points.Length; i++) {
			lineRenderer.SetPosition(i, points[i].position);
		}
	}
}
