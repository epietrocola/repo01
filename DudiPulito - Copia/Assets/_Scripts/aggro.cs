﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class aggro : MonoBehaviour {

	public bool canMove;

	CircleCollider2D aggroCollider;
	AILerp ai;
	bool isRunning;
	bool isPatrolling;
	public bool isPatrol;
	public bool randomPatrol;
	public int patrolIndex;
	public float checkTime;
	public Transform Target;
	public float maxAggroDistance;
	public float minAggroDistance;
	public Transform spawnPosition;
	public List<Transform> patrollingPoints;
	public float lostAggroDistance;
	public GameObject GameManager;
	public GameObject BossUI;


	// Use this for initialization
	void Start ()
	{

		GameManager = GameObject.FindGameObjectWithTag("GameManager");
		//BossUI = GameManager.GetComponent<BossManager>().BossFightUI;
		//aggroCollider = GetComponent<CircleCollider2D> ();
		ai = GetComponent<AILerp> ();
		Target = GameObject.FindGameObjectWithTag ("Player").transform;
		spawnPosition = transform.parent.GetComponent<Transform> ();
		//Debug.Log(GetComponentInParent<Transform>().name);
		if (isPatrol) {
			patrollingPoints = GameObject.FindGameObjectWithTag ("POI").GetComponent<pointsOfInterest> ().points;
			if (randomPatrol) {
				ai.target = patrollingPoints [Random.Range (0, patrollingPoints.Count - 1)];
			} else {
				ai.target = patrollingPoints [patrolIndex];
			}
		}
	}

	// Update is called once per frame
	void Update () {
		

		if (!isRunning) {
			StartCoroutine ("aggroCheck");
		}
		/*else {
			StartCoroutine("Patrol");
		}*/
//		Debug.Log (Vector2.Distance (Target.position, this.gameObject.transform.position));
	}

	/*void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			ai.enabled = true;
		}
	}*/

	IEnumerator aggroCheck ()
	{
		isRunning = true;
		yield return new WaitForSeconds (checkTime);
		if (Vector2.Distance (Target.position, this.gameObject.transform.position) < maxAggroDistance && Vector2.Distance (Target.position, this.gameObject.transform.position) > minAggroDistance) {
			if (canMove) {
				ai.enabled = true;
			}
			if (GetComponent<enemyHealth> ().isBoss) {
				BossUI.SetActive (true);
			}
		}
		//if (Vector2.Distance (Target.position, this.gameObject.transform.position) < minAggroDistance) {
		else if (Vector2.Distance (Target.position, this.gameObject.transform.position) < minAggroDistance) {
			if (canMove) {
				ai.enabled = false;
			}
			if (GetComponent<enemyHealth> ().isBoss) {
				//BossUI.SetActive (false);
			}
		}

		if (Vector2.Distance (Target.position, this.gameObject.transform.position) > lostAggroDistance) {
			//	ai.target = patrollingPoints [Random.Range (0, patrollingPoints.Count-1)];
			if (GetComponent<enemyHealth> ().isBoss) {
				BossUI.SetActive (false);
			}
			if (ai.targetReached) {
				if (isPatrol) {
					//FARE SIA RANDOM CHE SCALETTA DI PUNTI
					if (randomPatrol) {
						ai.target = patrollingPoints [Random.Range (0, patrollingPoints.Count - 1)];
					} else {
						if (patrolIndex < patrollingPoints.Count - 1) {
							patrolIndex++;
						}
						else patrolIndex = 0;

						ai.target = patrollingPoints [patrolIndex];

					}
				} else {
					ai.enabled = false;
				}
			}

		} 
		else {
			ai.target = Target;
		}
		isRunning = false;
	}

	IEnumerator Patrol(){
		isPatrolling = true;
		yield return new WaitForSeconds (checkTime);



		isPatrolling = false;
	}
}
