﻿using UnityEngine;
using System.Collections;

public class SpawnBoss : MonoBehaviour {

	public GameObject mob;
	public float spawnRate;
	GameObject _mob;
	int spawnCount = 0; 

	// Use this for initialization
	void Start () {
		_mob =	(GameObject)Instantiate (mob, transform.position, transform.rotation);
		_mob.transform.SetParent (this.gameObject.transform);
		GetComponent<SpriteRenderer> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (_mob != null /*&& spawnCount > 0*/) {
			return;
		} 
		else {
			StartCoroutine ("spawnNewMob");
			//spawnCount++;
		}
	}

	IEnumerator spawnNewMob(){
		_mob =	(GameObject)Instantiate (mob, transform.position, transform.rotation);
		_mob.transform.SetParent (this.gameObject.transform);
		_mob.SetActive (false);
		yield return new WaitForSeconds (spawnRate);
		_mob.SetActive (true);
	}
}
