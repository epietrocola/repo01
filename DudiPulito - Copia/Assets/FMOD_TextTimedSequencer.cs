﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FMOD_TextTimedSequencer : MonoBehaviour
{

    public float stepTime;
    public char[] elements;
    public AudioChar[] aChar;
    public StudioEventEmitter[] emitter;
    // Use this for initialization
    void Start()
    {
        StartCoroutine("Step");
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator Step()
    {
        foreach (var item in aChar)
        {
            PlayChar(item.character);
            yield return new WaitForSeconds(item.time);
            StopChar(item.character);
        }
    }

    void PlayChar(char character)
    {
        switch (character)
        {
            case 'a': emitter[0].enabled = true; break;
            case 'b': emitter[1].enabled = true; break;
            default:
                Debug.Log(character + " doesn't exist in this switch statement"); break;
        }
    }

    void StopChar(char character)
    {
        switch (character)
        {
            case 'a': emitter[0].enabled = false; break;
            case 'b': emitter[1].enabled = false; break;
            default:
                Debug.Log(character + " doesn't exist in this switch statement"); break;
        }
    }
}
