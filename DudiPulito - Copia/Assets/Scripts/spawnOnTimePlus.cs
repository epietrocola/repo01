﻿using UnityEngine;
using System.Collections;

public class spawnOnTimePlus : MonoBehaviour {

	public GameObject explosion;
	public float offset;
	public float particleSpeed;
	public bool isTrail;
	public float dimension;
	public float maxMultiple;
	public float ran;
	// Use this for initialization
	void Start () {
		//Debug.Log ("OH");
		//StartCoroutine ("spawnTime");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator spawnTime ()
	{
		yield return new WaitForSeconds (offset);	
		int i = 0;

		while (true) {
			
			GameObject trail = (GameObject) Instantiate (explosion, transform.position + transform.position * Random.Range(ran * -1f, ran) , Quaternion.AngleAxis(Random.Range(0.0f ,360.0f ), Vector3.forward));
			if (isTrail) {
				if(i < maxMultiple){
					i++;
				}
				trail.transform.localScale = new Vector3(i/maxMultiple*dimension, i/maxMultiple*dimension, 1f);
			}
			yield return new WaitForSeconds (particleSpeed);	
//			Debug.Log ("CICCIA");
		}
	}

	void OnDisable(){
		StopCoroutine("spawnTime");
		Instantiate (explosion, transform.position + transform.position * Random.Range(ran * -1f, ran) , Quaternion.AngleAxis(Random.Range(0.0f ,360.0f ), Vector3.forward));
	}

	void OnEnable(){
		StartCoroutine ("spawnTime");

	}
}
