﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BulletDamagePlayer : MonoBehaviour {

	public int minDamage;
	public int maxDamage;
	public int damage;
	public GameObject bulletExplosion;

	// Use this for initialization
	void Start () {
		damage = Random.Range (minDamage, maxDamage);
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Enemy" || other.tag == "Obstacle") {
			if (other.gameObject != this.gameObject) {
				//Debug.Log ("Si");
				Instantiate (bulletExplosion, this.gameObject.transform.position, Quaternion.identity);
				Destroy (gameObject);	
			}
		}
	}

}
