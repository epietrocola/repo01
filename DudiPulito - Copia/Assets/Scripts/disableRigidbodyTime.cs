﻿using UnityEngine;
using System.Collections;

public class disableRigidbodyTime : MonoBehaviour {

	public float timer;
    public Rigidbody2D col;

	// Use this for initialization
	void Start () {
		StartCoroutine ("disableRigidbody");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator disableRigidbody(){
		yield return new WaitForSeconds (timer);
        Debug.Log("stop");
        col = GetComponent<Rigidbody2D>();
        //col.isKinematic = true;
        //col.WakeUp();
        col.constraints = RigidbodyConstraints2D.FreezeAll;
    }
}
