﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Dropzone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {

	public bool hasCapacityLimits;
	public int capacityLimit;
	public bool isReady;

	public void OnPointerEnter (PointerEventData eventData){
		//Debug.Log("OnPointerEnter");

		if(eventData.pointerDrag == null)
			return;

		dragScript d = eventData.pointerDrag.GetComponent<dragScript> ();

		if (d != null) {
			d.placeholderParent = this.transform;
		}
	}

	public void OnPointerExit (PointerEventData eventData){
		//Debug.Log("OnPointerExit");
		if(eventData.pointerDrag == null)
			return;

		dragScript d = eventData.pointerDrag.GetComponent<dragScript> ();


		if (d != null && d.placeholderParent == this.transform) {
			d.placeholderParent = d.parentToReturnTo;
		}
	}

	public void OnDrop (PointerEventData eventData)
	{
		//Debug.Log ("OnDrop to " + gameObject.name);

		//QUANDO VIENE DISTRUTTO IL PLACEHOLDER childCount <= capacityLimit AMMETTE UN NUOVO CHILD, ERRORE

		//mmmm
		/*for (int i = 0; i < transform.childCount; i++) {
			if (transform.GetChild (i).GetComponent<Image> () == null && transform.childCount > 0) {
				isReady = false;
				return;
			} else {
				isReady = true;
			}
		}*/

		if (hasCapacityLimits && transform.childCount <= capacityLimit) {
			Debug.Log ("tette");

			if (eventData.pointerDrag == null)
				return;
			//object we're dragging around
			dragScript d = eventData.pointerDrag.GetComponent<dragScript> ();
			if (d != null) {
				d.parentToReturnTo = this.transform;
			}
		} else if(!hasCapacityLimits) {
			if (eventData.pointerDrag == null)
				return;
			//object we're dragging arount
			dragScript d = eventData.pointerDrag.GetComponent<dragScript> ();
			if (d != null) {
				d.parentToReturnTo = this.transform;
			}
		}
	}

}
