﻿using UnityEngine;
using System.Collections;

public class rotateLerp : MonoBehaviour {

	int count = 1;
	bool isRising;
	public int maxCount;
	public float speed;
	public float step;
	public float floatHeight;
    public float liftForce;
    public float damping;
    public Rigidbody2D rb2D;
    float up;
    float right;
    float down;
    float left;
	Vector3 euler;
	Vector3 oldEuler;
	//public int layer;
	public LayerMask ObstacleMask;

	// Use this for initialization
	void Start () {
		StartCoroutine("rotate");
		rb2D = GetComponent<Rigidbody2D>();


	}

	IEnumerator rotate ()
	{
		//Vector3 euler = //transform.eulerAngles;
		//euler.z = Random.Range(0f, 360f);
		//for (count = 1; count < maxCount; count++) {
		
		while (true) {
			if (oldEuler != euler) {
				count = 0;	
			}
			if (count >= maxCount) {
				isRising = false;	
			} else if (count <= 0) {
				isRising = true;
			}

			if (isRising) {
				count++;
			} else {
				count--;
			}
				//Debug.Log(count + " " + count/(float)maxCount);
			checkAxis();
			Debug.Log (euler);
			euler.z = euler.y;
			euler.y = 0;
			transform.eulerAngles = Vector3.Lerp (transform.rotation.eulerAngles, euler , count / (float)maxCount);
			oldEuler = euler;
			yield return new WaitForSeconds (step);

			}
	}


	float ray (Vector2 vector)
	{
		RaycastHit2D item = Physics2D.Raycast (transform.position, vector, Mathf.Infinity, ObstacleMask);
		//Debug.DrawRay(transform.position, vector*1000);
		//foreach (var item in hit) {
			if (item.collider != null) {
				//Debug.Log (item.collider.tag);
				if (item.collider.tag == "Obstacle") {
					float distance = Mathf.Abs (item.point.y - transform.position.y);
					//float heightError = floatHeight - distance;
					//Debug.Log("vector" + vector +" " +  distance + " " + hit.collider.name);
					//float force = liftForce * heightError - rb2D.velocity.y * damping;
					//rb2D.AddForce (Vector3.up * force);
					return distance;
				}
				return 9999;
			
		}
		return 10000;
	}

	void checkAxis(){
		up = ray (Vector2.up);
		right = ray (Vector2.right);
		down = ray (Vector2.down);
		left = ray (Vector2.left);
		//Debug.Log (up + " " + right + " " + down + " " + left); 
		if (up < right && up < down && up < left) {
			//up
			//euler = Quaternion.Euler(Vector3.up);
			euler = Vector2.up;


		} else if (right < up && right < down && right < left) {
			//right
			//euler = Quaternion.Euler(Vector3.right);
			euler = Vector2.right;


		} else if (left < up && left < down && right > left) {
			//left
			//euler = Quaternion.Euler(Vector3.left);
			euler = Vector2.left;


		} else if (down < up && left > down && down < right) {
			//down
			//euler = Quaternion.Euler(Vector3.down);
			euler = Vector2.down;

		}
	}
}
