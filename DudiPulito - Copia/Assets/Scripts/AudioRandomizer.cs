﻿using UnityEngine;
using System.Collections;

public class AudioRandomizer : MonoBehaviour {

	public bool isLoop;
	public float rate;
	public float minPitch, maxPitch;
	AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
		if (isLoop){
		 	StartCoroutine("timedPitch");
		}
		source.pitch = Random.Range(minPitch, maxPitch);

	}

	IEnumerator timedPitch(){
		if (!source.isPlaying) {
			source.pitch = Random.Range(minPitch, maxPitch);
		}
		yield return new WaitForSeconds(rate);
	}
}
