﻿using UnityEngine;
using System.Collections;

public class menu : MonoBehaviour {

	public GameObject Menu;
	public GameObject FPS;
	GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Cancel")) {
			Menu.SetActive (!Menu.activeSelf);
			if (Menu.activeSelf == true) {
				Time.timeScale = 0;
				player.GetComponent<Movement> ().enabled = false;
			} 
			else {
				Time.timeScale = 1;
				player.GetComponent<Movement> ().enabled = true;
			}
		}
	}

	public void Quits(){
		Debug.Log("Quit");
		Application.Quit ();
	}

	public void FPSActivate(){
		FPS.SetActive (!FPS.activeSelf);
	}
}
