﻿using UnityEngine;
using System.Collections;

public class melee : MonoBehaviour {

	public int minDamage;
	public int maxDamage;
	public int damage;
	public GameObject bulletExplosion;

	// Use this for initialization
	void Start () {
		damage = Random.Range (minDamage, maxDamage);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Enemy" || other.tag == "Obstacle") {
//			Debug.Log (other.name);
			if (other.gameObject != this.gameObject) {
				damage = Random.Range (minDamage, maxDamage);
//				Debug.Log ("Si");
				Instantiate (bulletExplosion, this.gameObject.transform.position, Quaternion.identity);
			}
		}
	}
}
