﻿using UnityEngine;
using System.Collections;

public class collActivatorTime : MonoBehaviour {

	//public Collider2D collider;
	public float time;


	// Use this for initialization
	void Start () {
		StartCoroutine ("activate");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator activate(){
		yield return new WaitForSeconds (time);
		GetComponent<Collider2D>().enabled = true;
	}
}
