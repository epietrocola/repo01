﻿using UnityEngine;
using System.Collections;

public class SpawnOnTime : MonoBehaviour {

	public GameObject explosion;
	public float offset;
	public float particleSpeed;
	public bool isTrail;
	public float dimension;
	public float maxMultiple;

	// Use this for initialization
	void Start () {
		//Debug.Log ("OH");
		//StartCoroutine ("spawnTime");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator spawnTime ()
	{
		yield return new WaitForSeconds (offset);	
		int i = 0;

		while (true) {
			
			GameObject trail = (GameObject) Instantiate (explosion, transform.position, Quaternion.identity);
			if (isTrail) {
				if(i < maxMultiple){
					i++;
				}
				trail.transform.localScale = new Vector3(i/maxMultiple*dimension, i/maxMultiple*dimension, 1f);
			}
			yield return new WaitForSeconds (particleSpeed);	
//			Debug.Log ("CICCIA");
		}
	}

	void OnDisable(){
		StopCoroutine("spawnTime");
	}

	void OnEnable(){
		StartCoroutine ("spawnTime");

	}
}
