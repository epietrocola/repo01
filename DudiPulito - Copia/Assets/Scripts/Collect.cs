﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Collect : MonoBehaviour {

	public int redPotions;
	public int coins;
	public Text potions;
	public Text coinsText;
	//public AudioClip[] AudioCoin;
	//public AudioClip[] AudioPotion;
	AudioSource source;
	bool isRunning;

    //audio
    [FMODUnity.EventRef]
    public string coinSoundEvent;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		//potions.text = redPotions.ToString ();
		//coinsText.text = coins.ToString();
//		Debug.Log (redPotions);
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "RedPotion") {
			if (redPotions < 10) {
				redPotions++;
                //playRandom(AudioPotion);
				Destroy (other.gameObject);
			}
		}
		else if (other.tag == "Coin") {
			coins++;
            //FMODUnity.RuntimeManager.PlayOneShot(coinSoundEvent); //TODO
            Destroy(other.gameObject);
		}

	}

	//Copy in OnTriggerStay if item has an inventory limit

	void OnTriggerStay2D (Collider2D other)
	{
		//if (!isRunning) {
		//StartCoroutine (triggerCheck (other, "RedPotion"));
		//}
		if (other.tag == "RedPotion") {
			if (redPotions < 10) {
				redPotions++;
				//playRandom (AudioPotion);
				Destroy (other.gameObject);
			}
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{

		if (other.tag == "RedPotion") {
			if (redPotions < 10) {
				redPotions++;
				//playRandom (AudioPotion);
				Destroy (other.gameObject);
			}
		}
	}

	void playRandom (AudioClip[] audio){
		source.pitch = Random.Range(0.8f, 1.2f);
		source.PlayOneShot(audio[Random.Range(0, audio.Length-1)]);
	}
}
