﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class destructible : MonoBehaviour {

	public int health;
	//public Image healthBar;
	//Vector3 scaleBar;
	//public GameObject lapide;
	//public Text score;
	public GameObject splinters;
	public GameObject Explosion;
	public GameObject damageUI;
	public GameObject spawnUI;
	public float strenght;
	public float strenghtDrop;
	public GameObject[] itemsDrop;
	public int min;
	public int max;
	public float ExplosionStrenght;
	public float torque;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		/*scaleBar = new Vector3 ((float)health/100, healthBar.gameObject.transform.localScale.y);
		healthBar.gameObject.transform.localScale = scaleBar;
		if (health <= 30) {
			healthBar.color = Color.red;
		} 
		else {
			healthBar.color = Color.green;
		}
		*/
		if (health <= 0) {
			//score.GetComponent<ScoreManager> ().points++;
			for (int i = 0; i < Random.Range(min, max) ; i++) {
				
				GameObject item = itemsDrop [Random.Range (0, itemsDrop.Length)];
				float rare = item.GetComponent<itemScript> ().rarity;
				if (rare > Random.Range(0, 100)){
					GameObject drop = (GameObject) Instantiate (item , gameObject.transform.position, gameObject.transform.rotation);
					Vector2 randDirDrop = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (-100, 100) / 100);
					drop.GetComponent<Rigidbody2D> ().AddForce (randDirDrop.normalized * strenghtDrop);
				}
			}
			for (int i = 0; i < Random.Range(5, 20); i++) {
				GameObject splinter = (GameObject) Instantiate(splinters, gameObject.transform.position, gameObject.transform.rotation);
				Vector2 randDirDrop = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (-100, 100) / 100);
				splinter.GetComponent<Rigidbody2D>().AddForce(randDirDrop.normalized * ExplosionStrenght);
				splinter.GetComponent<Rigidbody2D>().AddTorque(torque);
					
			}
			Instantiate (Explosion, gameObject.transform.position, gameObject.transform.rotation);
			Destroy (gameObject);
		}

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
			//			Debug.Log (other.GetComponent<Collider>().GetComponent<bulletDamage> ().damage);
			health -= other.GetComponent<BulletDamagePlayer> ().damage;
			GameObject UI = (GameObject) Instantiate (damageUI, spawnUI.transform.position, spawnUI.transform.rotation);
			//UI.transform.SetParent (gameObject.transform);
			UI.GetComponentInChildren<Text> ().text = other.GetComponent<BulletDamagePlayer> ().damage.ToString ();
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);

			//Destroy(other.gameObject);
		}

		if (other.tag == "Melee") {
			//Debug.Log (other.collider.GetComponent<bulletDamage> ().damage);
			health -= other.GetComponent<melee> ().damage;
			GameObject UI = (GameObject) Instantiate (damageUI, spawnUI.transform.position, spawnUI.transform.rotation);
			//UI.transform.SetParent (gameObject.transform);
			UI.GetComponentInChildren<Text> ().text = other.GetComponent<melee> ().damage.ToString ();
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);

			//Destroy(other.gameObject);
		}
	}
}
