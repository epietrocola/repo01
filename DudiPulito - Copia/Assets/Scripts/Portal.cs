﻿using UnityEngine;
using System.Collections;


public class Portal : MonoBehaviour {

	public Transform portalLink;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
//		Debug.Log ("Portal triggered");
		if (other.tag == "Player") {
			other.gameObject.transform.position = portalLink.position;
		}
	}
	void OnTriggerExit2d(){
		
	}
}
