﻿using UnityEngine;
using System.Collections;

public class backGroundAnimator : MonoBehaviour {

	int count;
	public int maxCount;
	bool isRising;
	public float speed;
	public float amount;
	Vector3 startPos;

	// Use this for initialization
	void Start () {
		startPos = transform.localPosition;
		StartCoroutine("anim");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator anim ()
	{
		while (true) {
			if (count >= maxCount) {
				isRising = false;	
			} else if (count <= 0) {
				isRising = true;
			}

			if (isRising) {
				count++;
			} else {
				count--;
			}

			transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 
				Mathf.Lerp((startPos.z) - (startPos.z * amount), (startPos.z) + (startPos.z * amount),  (float)count / (float)maxCount));
			yield return new WaitForSeconds (speed);
		}
	}
}
