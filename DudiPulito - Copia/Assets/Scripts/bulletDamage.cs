﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class bulletDamage : MonoBehaviour {

	public int minDamage;
	public int maxDamage;
	public int damage;
	public int parryDamage;
	public float strenght;

	public GameObject damageUI;
   	//public GameObject spawnUI;

	public GameObject bulletExplosion;

	// Use this for initialization
	void Start () {
		damage = Random.Range (minDamage, maxDamage);
		parryDamage = (int)( 20f * ((float)damage / 100f));

	}

	void OnTriggerEnter2D(Collider2D other){
//		Debug.Log (other.tag);
		if(other.tag == "Melee"){
			other.GetComponentInParent<Health> ().health -= parryDamage;
//			Debug.Log("Parry " + parryDamage);


			GameObject UI = (GameObject) Instantiate (damageUI, transform.position , Quaternion.identity);
			//UI.transform.SetParent (gameObject.transform);
			UI.GetComponentInChildren<Text> ().text = "Parry " + parryDamage.ToString ();
			//UI.GetComponentInChildren<Text> ().color = Color.blue;
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);

			Instantiate (bulletExplosion, this.gameObject.transform.position, Quaternion.identity);
			Destroy (gameObject);	
		}

		else if (other.tag == "Shield") {
			GameObject UI = (GameObject) Instantiate (damageUI, transform.position , Quaternion.identity);
			UI.GetComponentInChildren<Text> ().text = "Block";
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);
			Instantiate (bulletExplosion, this.gameObject.transform.position, Quaternion.identity);
			Destroy (gameObject);	
		}

		else if (other.tag == "Player" || other.tag == "Obstacle" ) {
			if (other.gameObject != this.gameObject) {
				Instantiate (bulletExplosion, this.gameObject.transform.position, Quaternion.identity);
				Destroy (gameObject);	
			}
		}
	}

}
