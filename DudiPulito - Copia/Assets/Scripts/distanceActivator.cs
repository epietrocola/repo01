﻿using UnityEngine;
using System.Collections;

public class distanceActivator : MonoBehaviour {

	public GameObject[] go;

	void OnTriggerEnter2D (Collider2D other)
	{
		for (int i = 0; i < go.Length; i++) {
			if (other.tag == "Player") {
				go[i].SetActive(true);
			}
		}

	}

	void OnTriggerExit2D (Collider2D other)
	{
		for (int i = 0; i < go.Length; i++) {
			if (other.tag == "Player") {
				go[i].SetActive(false);
			}
		}
	}

}
