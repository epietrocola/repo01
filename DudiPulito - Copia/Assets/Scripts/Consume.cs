﻿using UnityEngine;
using System.Collections;

public class Consume : MonoBehaviour {

	public int potionHealing;
	public AudioClip[] AudioHeal;
	AudioSource source;


	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)){
			int health = GetComponent<Health> ().health;
			int potions = GetComponent<Collect> ().redPotions;
			if (potions > 0 && health != 100) {
				if (100 - health > potionHealing) {
					GetComponent<Health> ().health = health + potionHealing;
					GetComponent<Collect> ().redPotions = potions - 1;
					GetComponent<Health> ().CharacterUI (potionHealing.ToString (), Color.green);
					//Debug.Log("OH");
				} 
				else {
					GetComponent<Health> ().CharacterUI ((100 - GetComponent<Health> ().health).ToString(), Color.green);
					GetComponent<Health> ().health = 100;
					GetComponent<Collect> ().redPotions = potions - 1;
				//	GetComponent<Health> ().CharacterUI (potionHealing.ToString ());
				}
				playRandom (AudioHeal);
			}
		}
	}

	void playRandom (AudioClip[] audio){
		if (source == null) {
			source = GetComponent<AudioSource>();

		}
		source.pitch = Random.Range(1f, 1.3f);
		source.PlayOneShot(audio[Random.Range(0, audio.Length)]);
	}
}
