﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using UnityEngine.Networking;
using FMOD;
using FMODUnity;

//public class Movement : NetworkBehaviour {
public class Movement :MonoBehaviour {

    

    public Animator anim; 

	public GameObject bulletPrefab;
	public GameObject bigBulletPrefab;
	public float force;
	public float bulletTime;
	public float bigBulletTime;

	public float speed;
	private Vector2 facing;
	private Vector2 direction;
	//[SyncVar (hook = "scaleHook")]
	public int scale;
	public float scaleAmount;
	public Vector3 scaleV;

	public Sprite Up;
	public Sprite Down;

	public Transform bulletPos;
	public Transform UpBull;
	public Transform DownBull;
	public Transform LeftBull;
	public Transform RightBull;

	//public Quaternion swordPos;

	public float run;
	public float walk;
	public float multiply;

	float timer;
	bool hasShot;
	public string equipped1;
	public string equipped2;

	public Vector3 mousePos;

	bool isSwinging;
	bool isBlocking;
	public float dist;
	public float swingSpeed;
	public float blockSpeed;

	public GameObject weaponMenu;
	public AudioClip AudioInventory;

    //AUDIO
    [FMODUnity.EventRef]
    public string footStepSound;
    FMOD.Studio.EventInstance ev;  
    FMOD.Studio.ParameterInstance par;

    

    void Start (){
		bulletPos = RightBull;
    }
    
	void Update ()
	{ 
        if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
        {
            FMOD.Studio.EventInstance ev = FMODUnity.RuntimeManager.CreateInstance(footStepSound);
            ev.getParameter("dai", out par);
            par.setValue(0.5f);
            ev.start();
            anim.SetBool("isRunning", true);
        }

        else
        {
            anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
            anim.SetFloat("Vertical", Input.GetAxis("Vertical"));
            anim.SetBool("isRunning", false);
        }

        //provare mettendo direction != vector.zero
        if (Input.GetAxis("Horizontal") > 0f)
        {
            //anim.SetFloat("Horizontal", Input.GetAxisRaw("Horizontal"));
            facing = Vector2.right;
            direction += Vector2.right;
            bulletPos = RightBull;
            anim.SetFloat("Horizontal", direction.magnitude);

        }
        else if (Input.GetAxis("Horizontal") == 0f)
        {
            //do nothing
            anim.SetFloat("Horizontal", Input.GetAxisRaw("Horizontal"));
            //anim.SetFloat("Horizontal", direction.magnitude);

        }
        else
        {
            //anim.SetFloat("Horizontal", Input.GetAxisRaw("Horizontal"));
            facing = Vector2.right;
            direction -= Vector2.right;
            bulletPos = RightBull;
            anim.SetFloat("Horizontal", direction.magnitude * -1f);

        }
        //}


        //if (Input.GetAxis("Vertical") != 0f)
        //{
        if (Input.GetAxis("Vertical") > 0f)
        {
            //anim.SetFloat("Vertical", Input.GetAxisRaw("Vertical"));
            facing = Vector2.up;
            direction += Vector2.up;
            GetComponent<SpriteRenderer>().sprite = Up;
            bulletPos = UpBull;
            anim.SetFloat("Vertical", direction.magnitude);
        }
        else if (Input.GetAxis("Vertical") == 0f)
        {
            //do nothing
            anim.SetFloat("Vertical", Input.GetAxisRaw("Vertical"));
            //anim.SetFloat("Vertical", direction.magnitude);

        }

        else
        {
            //anim.SetFloat("Vertical", Input.GetAxisRaw("Vertical"));
            facing = Vector2.down;
            direction -= Vector2.up;
            GetComponent<SpriteRenderer>().sprite = Down;
            bulletPos = DownBull;
            anim.SetFloat("Vertical", direction.magnitude * -1f);
        }
        //}


        /*if (Input.GetAxis("Horizontal") == 0f Input.GetAxis("Vertical") == 0f)
        {
            direction = Vector2.zero;
        }*/


        if (Input.GetKeyUp(KeyCode.W))
        {
            //Debug.Log (direction);
        }
        /*if (direction == Vector2.zero)
        {
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            GetComponent<Rigidbody2D>().angularVelocity = 0f;

        }
        else
        {

            GetComponent<Rigidbody2D>().isKinematic = false;
            GetComponent<Rigidbody2D>().AddForce(direction.normalized * speed * multiply * Time.deltaTime);
        }*/
        //Debug.Log(direction);
        if (direction == Vector2.zero)
        {
            //Debug.Log("direction == 0");
            //GetComponent<Rigidbody>().velocity = Vector3.zero;
            //GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            //GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            GetComponent<Rigidbody2D>().isKinematic = false;
            GetComponent<Rigidbody2D>().AddForce(direction.normalized * speed * multiply * Time.deltaTime);
        }

        //ev.setParameterValue("dai", 1f);
        if (Input.GetKeyDown(KeyCode.O))
        {

        }
        else if (Input.GetKeyDown(KeyCode.I))
        {

        }
        else if (Input.GetKeyDown(KeyCode.U))
        {

        }

        direction = Vector2.zero;
		/*if (!isLocalPlayer) {
			GetComponent<Rigidbody2D> ().isKinematic = true;
			return;
		}*/

		/*if (Input.GetKey(KeyCode.Alpha2)){
			equipped = "melee";
			melee.enabled = true;
			magic.enabled = false;
		}

		if (Input.GetKey (KeyCode.Alpha3)) {
			equipped = "magic";
			melee.enabled = false;
			magic.enabled = true;
		}*/

		mousePos = Input.mousePosition;
		mousePos.x -= Screen.width / 2;
		mousePos.y -= Screen.height / 2;

		if (Input.GetKey (KeyCode.LeftShift)) {
			speed = walk;
		} else {
			speed = run;
		}


		if (Input.GetButtonDown ("Fire1")) {
			if (equipped1 == "melee"  && isSwinging == false ) {
				Sword ();
			} else if (equipped1 == "magic") {
				hasShot = false;
				//Debug.Log (Screen.width + " X " + Screen.height);
			}else if (equipped1 == "shield" && isBlocking == false ) {
				Shield();
				//Debug.Log("calling");
			}
		} 


		if (Input.GetButton ("Fire1") && hasShot == false && equipped1 == "magic") {
			timer += Time.deltaTime;
			if (timer > 2f) {
				//Debug.Log ("BAMMA!");
				CmdFireBig ();
				timer = 0;
				hasShot = true;
			}
			else hasShot = false;
		}

		if (Input.GetButtonUp ("Fire1") && timer < 2f && hasShot == false && equipped1 == "magic") {
			CmdFire ();
			timer = 0;
		}




		if (Input.GetButtonDown ("Fire2") ) {
			if (equipped2 == "melee" && isSwinging == false) {
				Sword ();
			} else if (equipped2 == "magic") {
				hasShot = false;
				//Debug.Log(Screen.width + " X " + Screen.height);
				//mousePos.y -= Screen.height/2;
			}
			else if (equipped2 == "shield"  && isBlocking == false)  {
				Shield();
			}
		}
		if (Input.GetButton ("Fire2") && hasShot == false && equipped2 == "magic") {
			timer += Time.deltaTime;
			if (timer > 2f) {
				//Debug.Log ("BAMMA!");
				CmdFireBig ();
				timer = 0;
				hasShot = true;
			}
			else hasShot = false;
		}
		if (Input.GetButtonUp ("Fire2") && timer < 2f && hasShot == false && equipped2 == "magic") {
			CmdFire ();
			timer = 0;
		}






		if (Input.GetKeyDown (KeyCode.Tab) == true) {
			weaponMenu.SetActive (!weaponMenu.activeSelf);
			GetComponent<AudioSource>().PlayOneShot(AudioInventory);
		}

	}

	void Sword(){
		mouseDirSimple ();
		StartCoroutine ("swordSwing");
	}

	void Magic(){

	}

	void Shield(){
		mouseDirSimple ();
		StartCoroutine ("ShieldBlock");
	}

	/*void mouseDirSimpleBackup(){
		//mousePos.x -= Screen.width/2;  ///EHEHEHEHE
		//mousePos.y -= Screen.height/2;

		float mouseX = Mathf.Abs (mousePos.x);
		float mouseY = Mathf.Abs (mousePos.y);

		Debug.Log ("MOUSE  " + mousePos.x + " " + mousePos.y);
		Debug.Log ("MOUSEABS  " + mouseX + " " + mouseY);

		if (mouseY > mouseX  && mousePos.y > 0) {
			//up right
			//Debug.Log("UP");
			sword = UpSword;

			//sreturn Vector2.up;
		} else if (mouseX > mouseY && mousePos.x > 0) {
			//down right
			//Debug.Log("RIGHT");
			sword = RightSword;
			//return Vector2.right;
		} else if (mouseY > mouseX && mousePos.y < 0) {
			//down left
			sword = DownSword;
			//Debug.Log("DOWN");
			//return Vector2.down;
		} else if (mouseX > mouseY && mousePos.x < 0) {
			//up left
			sword = LeftSword;
			//Debug.Log("LEFT");
			//return Vector2.left;
		} else {

			//return Vector2.up;
		}
	}*/

	void mouseDirSimple(){
		//mousePos.x -= Screen.width/2;  ///EHEHEHEHE
		//mousePos.y -= Screen.height/2;

		// y > x/2 then 1
		// x/2 < y < x then 2
		// y < x/2 then 3

		float mouseX = Mathf.Abs (mousePos.x);
		float mouseY = Mathf.Abs (mousePos.y);

		//Debug.Log ("MOUSE  " + mousePos.x + " " + mousePos.y);
		//Debug.Log ("MOUSEABS  " + mouseX + " " + mouseY);

		//PROBABILMENTE LE DIVISIONI DEVONO ESSERE PER TERZI E NON PER MEZZI PER BILANCIARE IL RADDOPPIO DEGLI ASSI

		if (mouseY > mouseX  && mousePos.y > 0 && mousePos.x > 0) {
			//Debug.Log ("1");
			//Debug.Log ("up");
		} else if (mouseX/3 < mouseY && mouseY < mouseX && mousePos.y > 0 && mousePos.x > 0) {
			//Debug.Log ("2");
			//Debug.Log ("up/right");
		} else if (mouseY < mouseX && mousePos.y > 0 && mousePos.x > 0) {
			//Debug.Log ("3");
			//Debug.Log ("right");
		} else if (mouseY < mouseX && mousePos.y < 0 && mousePos.x > 0) {
			//Debug.Log ("4");
			//Debug.Log ("right");
		} else if (mouseY/3 < mouseX && mouseX < mouseY && mousePos.y < 0 && mousePos.x > 0) {
			//Debug.Log ("5");
			//Debug.Log ("down/right");
		} else if (mouseY > mouseX  && mousePos.y < 0 && mousePos.x > 0) {
			//Debug.Log ("6");
			//Debug.Log ("down");
		} else if (mouseY < mouseX && mousePos.y < 0 && mousePos.x < 0) {
			//Debug.Log ("9");
			//Debug.Log ("left");
		} else if (mouseY/3 < mouseX && mouseX < mouseY && mousePos.y < 0 && mousePos.x < 0) {
			//Debug.Log ("8");
			//Debug.Log ("down/Left");
		} else if (mouseY > mouseX  && mousePos.y < 0 && mousePos.x < 0) {
			//Debug.Log ("7");
			//Debug.Log ("down");

		} else if (mouseY > mouseX  && mousePos.y > 0 && mousePos.x < 0) {
			//Debug.Log ("12");
			//Debug.Log ("up");
		} else if (mouseX/3 < mouseY && mouseY < mouseX && mousePos.y > 0 && mousePos.x < 0) {
			//Debug.Log ("11");
		} else if (mouseY < mouseX && mousePos.y > 0 && mousePos.x < 0) {
			//Debug.Log ("10");
			//Debug.Log ("left");
		}
	}

	IEnumerator swordSwing(){
		//Debug.Log("WE" +  mouseDirSimple());
		//mousePos.x -= Screen.width/2;
		//mousePos.y -= Screen.height/2;
		/*UpSword.transform.rotation = upswordtrans;
		DownSword.transform.rotation = downswordtrans;
		LeftSword.transform.rotation = leftswordtrans;
		RightSword.transform.rotation = rightswordtrans;*/
		//Debug.Log (mousePos);
		isSwinging = true;
		//sword.SetActive (true);
		//Vector3 dir = Vector3.Normalize(mousePos - this.gameObject.transform.position);
		//Vector2 potd = mouseDirSimple();
		//Vector2 dir = Vector2.Scale(potd, mouseDirSimple());
		//GameObject swords = (GameObject)Instantiate(sword, potd, Quaternion.identity);
		//swords.transform.SetParent (this.gameObject.transform);
		//for(int i = 0; i < 3; i++){
			//sword.transform.localRotation = Quaternion.Lerp(sword.transform.rotation, Quaternion.AngleAxis(45, Vector2.up), i * Time.deltaTime);
		//	Debug.Log(i* Time.deltaTime);
			yield return new WaitForSeconds (swingSpeed);

		//}

		/*UpSword.SetActive (false);
		DownSword.SetActive (false);
		LeftSword.SetActive (false);
		RightSword.SetActive (false);
		UpRightSword.SetActive (false);
		DownRightSword.SetActive (false);
		UpLeftSword.SetActive (false);
		DownLeftSword.SetActive (false);*/

		//Debug.Log ("SUCCESSO");
		//sword.transform.rotation = swordPos;
		//Destroy(swords);
		isSwinging = false;

		//elapsedTime += Time.deltaTime;
	}

	IEnumerator ShieldBlock(){
		isBlocking = true;
		//shield.SetActive(true);

		yield return new WaitForSeconds (blockSpeed);

		/*UpShield.SetActive (false);
		DownShield.SetActive (false);
		LeftShield.SetActive (false);
		RightShield.SetActive (false);
		UpRightShield.SetActive (false);
		DownRightShield.SetActive (false);
		UpLeftShield.SetActive (false);
		DownLeftShield.SetActive (false);*/

		isBlocking = false;
	}

		


	//Non funziona in multiplayer perché in questo comando bulletPos non è in sync con il server.
	//[Command]
	void CmdFire(){
		//mousePos.x -= Screen.width/2;
		//mousePos.y -= Screen.height/2;


		//GameObject bullet = (GameObject)Instantiate (bulletPrefab, bulletPos.position, bulletPos.rotation);
		GameObject bullet = (GameObject)Instantiate (bulletPrefab, this.gameObject.transform.position, bulletPos.rotation);
		//bullet.GetComponent<Rigidbody2D> ().AddForce (facing * force);
		//Vector3 dir = Vector3.Normalize(mousePos - this.gameObject.transform.position);
		//Vector3 dir = mousePos.normalized - this.gameObject.transform.position.normalized;
		//	Vector3 center = new Vector3(Screen.width/2, Screen.height/2, 0);
		Vector3 dir = Vector3.Normalize(mousePos);
	//	dir.Normalize();
		bullet.GetComponent<Rigidbody2D> ().AddForce (dir * force);
		//Debug.Log("Mousepos " + mousePos.normalized);
		//Debug.Log("Position " + this.gameObject.transform.position.normalized );
		//Debug.Log ("Dir " +dir);
		//Debug.Log ("1" + dir);
		//Debug.Log("2" + mousePos);
		//NetworkServer.Spawn (bullet);
		Destroy (bullet, bulletTime);
	}

	void CmdFireBig(){
		//mousePos.x -= Screen.width/2;
		//mousePos.y -= Screen.height/2;
		//GameObject bullet = (GameObject)Instantiate (bulletPrefab, bulletPos.position, bulletPos.rotation);
		GameObject bullet = (GameObject)Instantiate (bigBulletPrefab, this.gameObject.transform.position, bulletPos.rotation);
		//bullet.GetComponent<Rigidbody2D> ().AddForce (facing * force);
		Vector3 dir = Vector3.Normalize(mousePos);
		//	dir.Normalize();
		bullet.transform.localScale = new Vector2(2, 2);
		bullet.GetComponent<Rigidbody2D> ().AddForce (dir * force);
		//Debug.Log ("1" + dir);
		//Debug.Log("2" + mousePos);
		//NetworkServer.Spawn (bullet);
		Destroy (bullet, bigBulletTime);
	}



	/* void scaleHook(int hook){
		Debug.Log ("Hook called");
		gameObject.transform.localScale = scale;
	} */
}
