﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class openGate : MonoBehaviour {

	public GameObject[] puzzle;
	public bool isPuzzle;
	public bool[] isSolved;
	public bool areSolved;
	bool isFirst = true;
	public GameObject[] activators;
	//ID of the puzzle to be opened by this activator, to be automated somehow
	public int ID;
	public int counter;

	public GameObject[] obj;
	public Transform[] pos;
	public float speed;
	int i;
	public int steps;
	public bool isOpen;
	public bool isRunning;
	public bool isIn;
	public Sprite spriteOn, spriteOff;
	List<Vector2> startPos = new List<Vector2>();
	//Vector2 startPos;


	// Use this for initialization
	void Start () {

		isSolved = new bool[puzzle.Length];
		for (int i = 0; i < obj.Length; i++) {
			startPos.Add(obj[i].transform.position);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{

		counter = 0;
		for (int i = 0; i < puzzle.Length; i++) {
			if (isSolved[i] == true) {
				counter++;
			}
		}

		if (counter >= puzzle.Length) {
			areSolved = true;
		}
		
		if (Input.GetKeyDown (KeyCode.E) && !isRunning && isIn && !isPuzzle) {
			StartCoroutine ("gateLerp");
		} 
		else if (Input.GetKeyDown (KeyCode.E) && !isRunning && isIn && isPuzzle && !areSolved) {
			puzzle[ID].SetActive (!puzzle[ID].activeSelf);
		}
		else if (!isRunning && isIn && isPuzzle && areSolved && isFirst) {
			puzzle[ID].SetActive(false);
			isFirst = false;
			StartCoroutine ("gateLerp");
		}
		else if (Input.GetKeyDown (KeyCode.E) && !isRunning && isIn && isPuzzle && areSolved && !isFirst) {
			StartCoroutine ("gateLerp");
		}

	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player") {
			isIn = true;
		}
	}
	void OnTriggerExit2D (Collider2D other)
	{
		if (other.tag == "Player") {
			isIn = false;
			puzzle[ID].SetActive(false);
		}
	}

	IEnumerator gateLerp ()
	{
		GetComponent<AudioSource> ().Play ();
		GetComponentInChildren<Light> ().intensity = GetComponentInChildren<Light> ().intensity * 2f;
		isRunning = true;
		//Debug.Log ("counter has been called");
		for (int i = 0; i < obj.Length; i++) {
			obj [i].GetComponent<AudioSource> ().Play ();
			obj [i].GetComponent<SpawnOnTime> ().enabled = true;
		}
		if (!isOpen) {	
			for (int n = 0; n <= activators.Length - 1; n++) {
				activators[n].GetComponent<SpriteRenderer> ().sprite = activators[n].GetComponent<openGate>().spriteOff;
				GetComponent<SpriteRenderer> ().sprite = spriteOff;
			}
			for (i = 0; i <= steps; i++) {
				for (int j = 0; j < obj.Length; j++) {
					obj [j].transform.position = Vector2.Lerp (startPos [j], pos [j].position, ((float)i / (float)steps)); 
				}
				yield return new WaitForSeconds (speed);
			}
		} else {
			for (int n = 0; n <= activators.Length - 1; n++) {
				activators[n].GetComponent<SpriteRenderer> ().sprite = activators[n].GetComponent<openGate>().spriteOn;
				GetComponent<SpriteRenderer> ().sprite = spriteOn;

			}
			for (i = steps; i >= 0; i--) {
				for (int j = 0; j < obj.Length; j++) {
					obj[j].transform.position = Vector2.Lerp (startPos[j], pos[j].position, ((float)i / (float)steps)); 
				}
				yield return new WaitForSeconds (speed);
			}
		}

		for (int i = 0; i < obj.Length; i++) {
			//obj[i].GetComponent<AudioSource>().Play();
			obj[i].GetComponent<SpawnOnTime>().enabled = false;
		}

		GetComponentInChildren<Light>().intensity = GetComponentInChildren<Light>().intensity / 2f ;

		for (int i = 0; i <= activators.Length -1; i++) {
			activators[i].GetComponent<openGate>().isOpen = !isOpen;
			activators[i].GetComponent<openGate>().isFirst = isFirst;
		}

		isOpen = !isOpen;
		isRunning = false;
	}

}
