﻿using UnityEngine;
using System.Collections;

public class SpawnOnTimeController : MonoBehaviour {

	public spawnOnTimePlus sot;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0) {
			sot.enabled = true;
		} else {
			sot.enabled = false;
		}
	}
}
