﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour {

	public int health; 
	public Image healthBar;
	Vector3 scaleBar;
	public GameObject damageUI;
	public GameObject spawnUI;
	public float strenght;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		scaleBar = new Vector3 ((float)health/100, healthBar.gameObject.transform.localScale.y);
		healthBar.gameObject.transform.localScale = scaleBar;
		if (health <= 30) {
			//healthBar.color = Color.red;
		} 
		else {
			//healthBar.color = Color.green;
		}

		if (health >= 100) {
			health = 100;
		}

		if (health <= 0) {
			health = 0;
			//score.GetComponent<ScoreManager> ().points++;
			//Instantiate (lapide, gameObject.transform.position, gameObject.transform.rotation);
			//Destroy (gameObject);
//				Debug.Log("MORTO");
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "EnemyBullet") {
			//Debug.Log (other.GetComponent<bulletDamage> ().damage);
			health -= other.GetComponent<bulletDamage> ().damage;
			//Destroy(other.GetComponent<Collider>().gameObject);

			GameObject UI = (GameObject) Instantiate (damageUI, spawnUI.transform.position, spawnUI.transform.rotation);
			//UI.transform.SetParent (gameObject.transform);
			UI.GetComponentInChildren<Text> ().text = other.GetComponent<bulletDamage> ().damage.ToString ();
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);
		}
	}

	public void CharacterUI(string str, Color col){
		GameObject UI = (GameObject) Instantiate (damageUI, spawnUI.transform.position, spawnUI.transform.rotation);
		UI.GetComponentInChildren<Text> ().text = str;
		UI.GetComponentInChildren<Text> ().color = col;
		Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
		UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);
	}
}
