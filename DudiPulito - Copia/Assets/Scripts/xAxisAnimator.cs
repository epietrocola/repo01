﻿using UnityEngine;
using System.Collections;

public class xAxisAnimator : MonoBehaviour {

	int count;
	public int maxCount;
	bool isRising;
	public float speed;
	public float amount;
	Vector3 startPos;

	// Use this for initialization
	void Start () {
		startPos = transform.localPosition;
		StartCoroutine("anim");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator anim ()
	{
		while (true) {
			if (count >= maxCount) {
				isRising = false;	
			} else if (count <= 0) {
				isRising = true;
			}

			if (isRising) {
				count++;
			} else {
				count--;
			}

			transform.localPosition = new Vector3 (Mathf.Lerp((startPos.x) - (startPos.x * amount), (startPos.x) + (startPos.x * amount),  (float)count / (float)maxCount), transform.localPosition.y, 
				transform.localPosition.z);
			yield return new WaitForSeconds (speed);
		}
	}
}
