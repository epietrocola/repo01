﻿using UnityEngine;
using System.Collections;

public class raycastTerrain : MonoBehaviour {

	//public LayerMask terrain;
	public Transform tr;
	public bool isOnDirt;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		RaycastHit2D[] hit2D;
		hit2D = Physics2D.RaycastAll (tr.position, Vector3.forward, Mathf.Infinity);
		if (hit2D != null) {
			for (int i = 0; i < hit2D.LongLength; i++) {
				if (hit2D [i].collider.tag == "Dirt") {
					isOnDirt = true;
				} else {
					isOnDirt = false;
				}
			}
		}

		if (!isOnDirt && (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0)) {
			GetComponent<spawnOnTimePlus> ().enabled = true;	
		} else {
			GetComponent<spawnOnTimePlus> ().enabled = false;	
		}
	}
}
