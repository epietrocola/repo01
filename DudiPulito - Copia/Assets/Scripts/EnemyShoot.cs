﻿using UnityEngine;
using System.Collections;

public class EnemyShoot : MonoBehaviour {

	public float maxDistance;
	public Transform target;
	public GameObject bullet;
	bool isrunning;
	public float shootingRate;
	public float forceAmount;
	Vector3 posit;
	public Light chargingLight;
	public float muzzleLightIntensity;
	public int steps;
	public float attackSpeed;
	public float releaseSpeed;
	public AudioClip chargeSound;
	public AudioClip shotSound;
	public float volume;


	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector2.Distance (this.gameObject.transform.position, target.position) < maxDistance && isrunning == false) {
			//GameObject bull = (GameObject) Instantiate (bullet, this.gameObject.transform.position, Quaternion.identity);
			StartCoroutine ("loadMove");
		}
	}

	IEnumerator shoot(){
		for (int i = 0; i < 5; i++) {
			//Debug.Log ("Shooting");
			//posit = new Vector3 (target.position.x - this.gameObject.transform.position.x, 0 , target.position.y - this.gameObject.transform.position.y);
			GameObject bull = (GameObject) Instantiate (bullet, this.gameObject.transform.position + Vector3.one, Quaternion.identity);	
			bull.GetComponent<Rigidbody2D>().AddForce((target.position - this.gameObject.transform.position)* forceAmount);
			//Debug.Log (posit);
			float ran = Random.Range(shootingRate - shootingRate * 80f / 100f,  shootingRate + shootingRate * 80f / 100f);
			//Debug.Log(ran);
			yield return new WaitForSeconds (ran);
			//Debug.Log(shootingRate + " "+  shootingRate * 50f / 100f);
			//Debug.Log(Random.Range(shootingRate - shootingRate * 50f / 100f,  shootingRate + shootingRate * 50f / 100f));
		}
	}

	IEnumerator loadMove ()
	{
		isrunning = true;
		GetComponent<AudioSource> ().PlayOneShot (chargeSound);
		for (int i = 0; i < steps; i++) {
			GetComponent<AudioSource> ().volume = (float)i / (float)steps;
			yield return new WaitForSeconds (attackSpeed);
			chargingLight.intensity = Mathf.Lerp (0f, muzzleLightIntensity, (float)i / (float)steps); 
		}
		GetComponent<AudioSource> ().Stop ();
		StartCoroutine ("shoot");
		GetComponent<AudioSource> ().PlayOneShot (shotSound, volume);

		for (int i = steps; i > 0; i--) {
			yield return new WaitForSeconds (releaseSpeed);
			chargingLight.intensity = Mathf.Lerp (0f, muzzleLightIntensity, (float)i / (float)steps); 
		}
		if (chargingLight != null) {
			chargingLight.intensity = 0;
		}
		float ran = Random.Range(shootingRate - shootingRate * 80f / 100f,  shootingRate + shootingRate * 80f / 100f);
		yield return new WaitForSeconds(ran);

		isrunning = false;

	}

}


