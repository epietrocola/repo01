﻿using UnityEngine;
using System.Collections;

public class minionManager : MonoBehaviour {

	Vector3 startPos;
	public Transform pos;
	int i;
	public int steps;
	public float speed;

	// Use this for initialization
	void Start () {
		startPos = transform.localPosition;
		StartCoroutine("sin");
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = Vector3.Lerp(pos.localPosition, startPos, (float)i/(float)steps);
	}

	IEnumerator sin ()
	{
		i = Random.Range(0, steps);
		while (true) {
			for ( ; i <= steps; i++) {	
				yield return new WaitForSeconds (speed);
				//Debug.Log(i);
			}
			for (i = steps; i >= 0; i--) {	
				yield return new WaitForSeconds (speed);
				//Debug.Log(i);
			}
		}
	}
}
