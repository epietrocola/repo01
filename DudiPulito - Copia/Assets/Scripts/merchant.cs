﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class merchant : MonoBehaviour {

	public GameObject merchGUI;
	public GameObject merchPanel;
	public GameObject itemUIPrefab;
	public GameObject[] items;
	public Text nameItem;
	public Text priceItem;
	public Image itemImage;
	public float seconds;
	public int itemNumber;
	int n;
	bool isIn;


	//public bool isOpened;

	// Use this for initialization
	void Start () {
		addMultipleItems();
		StartCoroutine ("changeMerch");
	}
	
	// Update is called once per frame
	void Update () {

		if (merchPanel.transform.childCount == 0) {
			merchGUI.SetActive (false);
		}

		if (Input.GetKeyDown (KeyCode.E) && isIn == true && merchPanel.transform.childCount != 0) {
			merchGUI.SetActive (!merchGUI.activeSelf);
			/*if (merchGUI.activeSelf == true) {
				GetComponentInChildren<buttonScript> ().checkMerch ();
			}*/

		}



		/*if (merchGUI.activeSelf == true && isIn == true) {
			GetComponentInChildren<buttonScript> ().checkMerch ();
		}*/
//		Debug.Log (items [0]);

	}

	void addMultipleItems(){
		for (int i = 0; i < itemNumber; i++) {
			n = Random.Range (0, items.Length);
			addItemMerch ();
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			isIn = true;

		}

	}

	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") {
			merchGUI.SetActive (false);
			isIn = false;
		}
	}
		

	void addItemMerch(){
		GameObject item = (GameObject)Instantiate (itemUIPrefab);
		nameItem.text = items [n].name;
		nameItem.name = items [n].name;
		priceItem.text = items[n].GetComponent<itemScript>().price.ToString();
		itemImage.sprite = items [n].GetComponent<SpriteRenderer> ().sprite;
		item.transform.SetParent (merchPanel.transform);
		item.transform.localScale = Vector2.one;
	}

	public void removeItemMerch(GameObject go){
		Destroy (go);
	}

	IEnumerator changeMerch(){
		while (true) {
			yield return new WaitForSeconds (seconds);
			if (!merchGUI.activeSelf) {
				for (int i = 0; i < merchPanel.transform.childCount; i++) {
					Destroy(merchPanel.transform.GetChild(i).gameObject);
				}
				for (int i = 0; i < itemNumber; i++) {
					n = Random.Range (0, items.Length);
					addItemMerch ();
				}
			}
		}
	}
}
