﻿using UnityEngine;
using System.Collections;

public class TransparentShadow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
