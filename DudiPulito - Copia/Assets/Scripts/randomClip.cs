﻿using UnityEngine;
using System.Collections;

public class randomClip : MonoBehaviour {

	AudioSource source;
	public AudioClip[] clips;
	// Use this for initialization
	void Start () {
		playRandom(clips);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void playRandom (AudioClip[] audio){
		if (source == null) {
			source = GetComponent<AudioSource>();

		}
		//source.pitch = Random.Range(1f, 1.3f);
		source.PlayOneShot(audio[Random.Range(0, audio.Length)]);
	}
}
