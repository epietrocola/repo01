﻿using UnityEngine;
using System.Collections;

public class sizeOnTime : MonoBehaviour {

	public int max;
	public int stepScale, stepAlpha;
	public float mult;
	public float time;
	public float startAlpha, endAlpha;
	public float startScale, endScale;

	// Use this for initialization
	void Start () {
		StartCoroutine("setScale");
		StartCoroutine("setAlpha");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator setScale(){
		for (int i = 0; i <= stepScale; i++) {
			Vector3 scale1 = new Vector3 (startScale, startScale, 1);
			Vector3 scale2 = new Vector3 (endScale, endScale, 1);
			gameObject.transform.localScale = Vector3.Lerp(scale1, scale2 , (float)i/(float)stepScale);
			//Color col = GetComponent<SpriteRenderer>().color;
			//col.a = Mathf.Lerp(startAlpha, endAlpha, (float)i / (float)stepScale);
			//Debug.Log ((float)i / (float)stepScale);
			//GetComponent<SpriteRenderer>().color = col;
			yield return new WaitForSeconds(time);
		}
	}

	IEnumerator setAlpha(){
		for (int i = 0; i <= stepAlpha; i++) {
			//Vector3 scale1 = new Vector3 (startScale, startScale, 1);
			//Vector3 scale2 = new Vector3 (endScale, endScale, 1);
			//gameObject.transform.localScale = Vector3.Lerp(scale1, scale2 , (float)i/(float)stepAlpha);
			Color col = GetComponent<SpriteRenderer>().color;
			col.a = Mathf.Lerp(startAlpha, endAlpha, (float)i / (float)stepAlpha);
			//Debug.Log ((float)i / (float)stepAlpha);
			GetComponent<SpriteRenderer>().color = col;
			yield return new WaitForSeconds(time);
		}
	}
}
