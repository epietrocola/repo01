﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class enemyHealth : MonoBehaviour {

	public bool isBoss;
	public string BossName;
	public int health;
	public int maxHealth;
	public Image healthBar;
	Vector3 scaleBar;
	public GameObject lapide;
	public Text score;
	public GameObject damageUI;
	public GameObject spawnUI;
	public float strenght;
	public float strenghtDrop;
	public GameObject[] itemsDrop;
	public AudioClip[] hit;
	public AudioClip[] death;
	public int min;
	public int max;
	private AudioSource source;
	float startX;
	GameObject gameManager;


	// Use this for initialization
	void Start ()
	{
		source = GetComponent<AudioSource> ();
		if (isBoss) {
			gameManager = GameObject.FindWithTag ("GameManager");
			//spostare questo codice nella funzione che si lancia a inizio boss fight
			gameManager.GetComponent<BossManager> ().BossName.text = BossName;
			healthBar = gameManager.GetComponent<BossManager> ().BossHealthBar;
			startX = healthBar.transform.localScale.x;

		} 
		else {
			startX = healthBar.transform.localScale.x;
		}

	}

	// Update is called once per frame
	void Update () {
		//Debug.Log(startX);
		//scaleBar = new Vector3 (((float)health/maxHealth) * startX, healthBar.gameObject.transform.localScale.y, 1);
		scaleBar = new Vector3 (Mathf.Lerp(0, startX, (float)health/(float)maxHealth), healthBar.gameObject.transform.localScale.y, 1);
		//healthBar.gameObject.transform.localScale = scaleBar;
		//Debug.Log((float)health/(float)maxHealth);
		healthBar.transform.localScale = scaleBar;
		if (health <= 30) {
			//healthBar.color = Color.red;
		} 
		else {
			//healthBar.color = Color.green;
		}

		if (health <= 0) {
			//score.GetComponent<ScoreManager> ().points++;
			for (int i = 0; i < Random.Range(min, max) ; i++) {
				GameObject item = itemsDrop [Random.Range (0, itemsDrop.Length)];
				float rare = item.GetComponent<itemScript> ().rarity;
				if (rare > Random.Range(0, 100)){
					GameObject drop = (GameObject) Instantiate (item , gameObject.transform.position, gameObject.transform.rotation);
					Vector2 randDirDrop = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (-100, 100) / 100);
					drop.GetComponent<Rigidbody2D> ().AddForce (randDirDrop.normalized * strenghtDrop);
				}
			}


			playRandom(death);
			Instantiate (lapide, gameObject.transform.position, gameObject.transform.rotation);
			Destroy (gameObject);
		}

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
//			Debug.Log (other.GetComponent<Collider>().GetComponent<bulletDamage> ().damage);
			health -= other.GetComponent<BulletDamagePlayer> ().damage;
			GameObject UI = (GameObject) Instantiate (damageUI, spawnUI.transform.position, spawnUI.transform.rotation);
			//UI.transform.SetParent (gameObject.transform);
			UI.GetComponentInChildren<Text> ().text = other.GetComponent<BulletDamagePlayer> ().damage.ToString ();
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);
			playRandom(hit);
			//Destroy(other.gameObject);
		}

		if (other.tag == "Melee") {
			//Debug.Log (other.collider.GetComponent<bulletDamage> ().damage);
			health -= other.GetComponent<melee> ().damage;
			GameObject UI = (GameObject) Instantiate (damageUI, spawnUI.transform.position, spawnUI.transform.rotation);
			//UI.transform.SetParent (gameObject.transform);
			UI.GetComponentInChildren<Text> ().text = other.GetComponent<melee> ().damage.ToString ();
			Vector2 randDir = new Vector2 (((float)Random.Range (-100, 100) / 100), (float)Random.Range (0, 100) / 100);
			UI.GetComponent<Rigidbody2D> ().AddForce (randDir * strenght);
			playRandom(hit);
			//Destroy(other.gameObject);
		}
	}

	void playRandom (AudioClip[] audio){
		source.pitch = Random.Range(0.8f, 1.2f);
		source.PlayOneShot(audio[Random.Range(0, audio.Length-1)]);
	}
}
