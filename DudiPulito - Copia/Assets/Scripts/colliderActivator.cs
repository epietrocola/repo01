﻿using UnityEngine;
using System.Collections;

public class colliderActivator : MonoBehaviour {

	public float seconds;
	public Collider2D coll;

	// Use this for initialization
	void Start () {
		coll = GetComponent<Collider2D> ();
		coll.enabled = false;
		StartCoroutine ("activate");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator activate(){
		yield return new WaitForSeconds (seconds);
		coll.enabled = true;
	}
}
