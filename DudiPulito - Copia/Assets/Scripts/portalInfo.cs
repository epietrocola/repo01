﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class portalInfo : MonoBehaviour {

	public string portalInfos;
	public Text infoGui;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Player") {
			infoGui.enabled = true;
			infoGui.text = portalInfos;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Player") {
			infoGui.enabled = false;
		}
	}
}
