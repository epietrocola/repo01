﻿using UnityEngine;
using System.Collections;

public class lightOverlapFix : MonoBehaviour {

	public static int lightCount;
	public float speed;
	public float min, max;
	public int maxLights;
	public int envelopeSteps;
	public bool destroy;
	public float destroyTimer;
	bool isRunning = false;

	// Use this for initialization
	void Start () {
		lightCount++;
		StartCoroutine("enlight");
		StartCoroutine("timeDestroyLight");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!isRunning) {
			GetComponent<Light> ().intensity = Mathf.Lerp (max, min, (float)lightCount / (float)maxLights);
		}
		//Debug.Log( "# Lights "+ lightCount + "intensity to " +  Mathf.Lerp(max, min, (float)lightCount/(float)maxLights));
//		Debug.Log(count/(float)div);
	}

	void OnDestroy(){
		lightCount--;
	}

	IEnumerator enlight ()
	{
		isRunning = true;
		for (int i = 0; i < envelopeSteps; i++) {
			GetComponent<Light>().intensity = i/(float)envelopeSteps;
			yield return new WaitForSeconds (speed);
		}
		isRunning = false;
	}

	IEnumerator timeDestroyLight ()
	{
		isRunning = true;
		yield return new WaitForSeconds (destroyTimer);
		for (int i = envelopeSteps; i > 0; i--) {
			GetComponent<Light> ().intensity = i / (float)envelopeSteps;
			yield return new WaitForSeconds (speed);
			//Debug.Log (i);
		}
		isRunning = false;
		if (destroy) {
			Destroy (gameObject.transform.parent.gameObject);
		}
	}
}
