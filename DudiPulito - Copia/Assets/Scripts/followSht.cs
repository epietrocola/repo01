﻿using UnityEngine;
using System.Collections;

public class followSht : MonoBehaviour {

	public GameObject target;
	public float maxDistance;
	public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector2.Distance(target.transform.position, this.gameObject.transform.position) < maxDistance){
			Vector2.Lerp(this.gameObject.transform.position, target.transform.position, speed*Time.time);
		}
	}
}
