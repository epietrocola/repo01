﻿using UnityEngine;
using System.Collections;

public class TorchScript : MonoBehaviour {

	public float speed;
	public float min;
	public float max;
	public int maxCount;
	public float lightMovement;
	public Light lg;
	int count = 0;
	bool isRising = true;
    // Use this for initialization

    void OnEnable () {
		lg = GetComponent<Light>();
		StartCoroutine("TorchLight");
		count = Random.Range(0, maxCount);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator TorchLight ()
	{
		while (true) {
			if (count >= maxCount) {
				isRising = false;	
			} else if (count <= 0) {
				isRising = true;
			}

			if (isRising) {
				count++;
			} else {
				count--;
			}


			//Debug.Log((float)count/(float)maxCount);
			//Debug.Log(count + " " + (float)(count/maxCount));
			lg.intensity = Mathf.Lerp (min, max, ((float)count/(float)maxCount));
			transform.localPosition = new Vector3(Mathf.Lerp(transform.localPosition.x - lightMovement, transform.localPosition.x + lightMovement, (float)count/(float)maxCount), transform.localPosition.y, transform.localPosition.z);
			yield return new WaitForSeconds (speed);
		}
	}

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
