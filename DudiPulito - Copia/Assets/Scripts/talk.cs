﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class talk : MonoBehaviour {

	public Text text;
	public string[] speech;
	public float seconds;
	public Image icon;
	public float lerpStepSeconds;
	Vector2 scaleIcon;
	float scaleTime;
	int previousRand;

	void Start () {
		if (icon != null) {
			icon.enabled = true;
		}
		text.enabled = false;
	}
	// Update is called once per frame
	void Update () {
	}
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") { 
//			Debug.Log ("Trigger");
			int rand = Random.Range(0, speech.Length);
//				Debug.Log ("rand " + rand);
			while (previousRand == rand) {
				rand = Random.Range(0, speech.Length);
//				Debug.Log ("while " + rand);
			}
			text.enabled = true;
			text.text = speech[rand];
			if (seconds != 0) {
				StartCoroutine ("waitFor");
			}
			if (icon != null) {
				icon.enabled = false;
				scaleTime = 0;
				StopCoroutine ("timeit");

			}
			previousRand = rand;
			} 
		}
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") { 
//			Debug.Log ("exit");
			text.enabled = false;
			if (seconds != 0) {
				StopCoroutine ("waitFor");
			}
			if (icon != null) {
				scaleIcon.x = 0.2f;
				scaleIcon.y = 0.2f;
				if (scaleTime == 0f) {
					StartCoroutine ("timeit");
				}
				icon.enabled = true;
			}
		}
	}
	IEnumerator timeit(){
		for (int i = 0; i < 10; i++) {
			scaleTime = i/10f;
//			print (scaleTime);
			icon.rectTransform.localScale = Vector2.Lerp(Vector2.zero, scaleIcon, scaleTime);
			yield return new WaitForSeconds (lerpStepSeconds);
		}
	}
	IEnumerator waitFor(){
		//Debug.Log ("Started waitFor");
		yield return new WaitForSeconds(seconds);
		text.enabled = false;
	}
}