﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;

public class SampleInput : MonoBehaviour
{
    //[FMODUnity.EventRef]
    //public string auSrc;


    //public FMOD.ChannelGroup group;

    public float volume;

    public int qSamples = 4096;
    private float[] samples;

    

    


    #region Singleton

    public static SampleInput Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion // Singleton

    // Use this for initialization
    void Start()
    {
        
        samples = new float[qSamples];
        FMOD.Studio.EventInstance inst;
        /*inst = FMOD.
        inst.getChannelGroup(out group);
        print(group);*/

    }

    // Update is called once per frame
    void Update()
    {
        volume = GetRMS(0) + GetRMS(1);
        //Debug.Log(volume * -1f + 1f);

    }

    float GetRMS(int channel)
    {
        //Replaced the AudioListener with the public AudioSource auSrc from above
        //auSrc.GetOutputData(samples, channel);
        float sum = 0;
        foreach (float f in samples)
        {
            sum += f * f;
        }
        return Mathf.Sqrt(sum / qSamples);
    }
}

