﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour {

    
    public FMODAudioReactiveLight[] enable;
    public TorchScript[] disable;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (var item in disable)
        {
            if (item.enabled != false)
            {
                item.enabled = false;
            }
        }

        foreach (var item in enable)
        {
            if (item.enabled != true)
            {
                item.enabled = true;
            }
        }                        
    }
}
