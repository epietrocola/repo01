﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOSwapper : MonoBehaviour {

    Texture[] frame;
    public Texture[] frameLeft;
    public Texture[] frameRight;
    public Texture[] frameUp;
    public Texture[] frameDown;
    public Texture[] frameIdle;

    public GameObject plane;
    public float speed;
    int i;
    int n = 0;
    public int direction;

    // Use this for initialization
    void Start () {
        frame = frameLeft;
        StartSwap();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.UP:
                frame = frameUp;
                break;
            case Direction.DOWN:
                frame = frameDown;
                break;
            case Direction.RIGHT:
                frame = frameRight;
                break;
            case Direction.LEFT:
                frame = frameLeft;
                break;
            case Direction.IDLE:
                frame = frameIdle;
                break;
            default:
                break;
        }
    }

    public void StartSwap()
    {
        StartCoroutine("GOSwapperCoroutine");
    }

    public void StopSwap()
    {
        StopAllCoroutines();
    }

    IEnumerator GOSwapperCoroutine()
    {

        while (true)
        {
            i = frame.Length;
            n++;
            
            if (n >= i)
            {
                n = 0;
            }

            GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", frame[n]);

            yield return new WaitForSeconds(speed);
        }
    }
}
