﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FMOD;
using FMODUnity;

public class AudioLightFlicker : MonoBehaviour
{

    // the light itself (not being used yet)
    //public Light flickeringLight;

    // UI text to display the value
    //public Text flickerAudioVolText;

    // channel group and dsp
    FMOD.ChannelGroup channelGroup;
    FMOD.DSP channelDSP;

    // outputmeter
    FMOD.DSP_METERING_INFO Outputmeter;

    // setting up the fmod event
    [FMODUnity.EventRef]
    public string flickerAudio;

    FMOD.Studio.EventInstance flickerAudioEvent;

// Use this for initialization
    void Start()
    {

        print("Start");

        Outputmeter = new FMOD.DSP_METERING_INFO();

        

        // initilising the fmod event
        flickerAudioEvent = FMODUnity.RuntimeManager.CreateInstance(flickerAudio);

        // event play
        flickerAudioEvent.start();

        // getting the light (not being used yet)
        //flickeringLight = GetComponent ();

        // this is where things get sketchy

        // getting the channel group
        flickerAudioEvent.getChannelGroup(out channelGroup);

        // getting the head dsp (I think)
        //channelGroup.getDSP(0, out channelDSP);

        // turning metering on
        //channelDSP.setMeteringEnabled(false, true);

        print("Start2");

    }

    // Update is called once per frame
    void Update()
    {
        
        // getting the channel group
        flickerAudioEvent.getChannelGroup(out channelGroup);

        if (channelGroup != null)
        {
            // getting the head dsp (I think)
            channelGroup.getDSP(0, out channelDSP);

            // turning metering on
            channelDSP.setMeteringEnabled(false, true);

            // this might be where I’m going wrong – I get an error that says Outputmeter will always be null
            channelDSP.getMeteringInfo(null, Outputmeter);

            // getting the RMS level
            float outputRMS = Outputmeter.rmslevel[1];
            
            // displaying the value – currently just displays the default placeholder text rather than any number so not working it seems
            print(outputRMS);
        }
       
    }
}