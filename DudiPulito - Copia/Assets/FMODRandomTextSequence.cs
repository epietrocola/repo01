﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODRandomTextSequence : MonoBehaviour {

    public FMOD_TextTimedSequencerv2Light sequencer;
    public int length;
    public char[] sequence;
    string stringSequence;
    string st = "abcd"; //"abcdefghijklmnopqrstuvwxyz";


    // Use this for initialization
    void Start () {
        if (sequencer == null)
        {
            sequencer = GetComponent<FMOD_TextTimedSequencerv2Light>();
        }

        sequence = new char[length];

        for (int i = 0; i < length; i++)
        {
            if (i % 2 == 0)
            {
                sequence[i] = st[Random.Range(0, st.Length)];

            }
            else
            {
                sequence[i] = Random.Range(0, 10).ToString().ToCharArray()[0];
            }
        }

        sequencer.elements = sequence;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
