﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioChar
{
    public char character = ' ';
    public float time = 0;
}
