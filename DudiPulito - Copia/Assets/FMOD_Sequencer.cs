﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FMOD_Sequencer : MonoBehaviour {

    public float stepTime;
    public StudioEventEmitter[] emitter;
	// Use this for initialization
	void Start () {
        StartCoroutine("Step");
    }
	
	// Update is called once per frame
	void Update () {
    }

    IEnumerator Step()
    {
        for (int i = 0; i < emitter.Length; i++)
        {
            print(i);
            foreach (var item in emitter)
            {
                item.enabled = false;
            }
            emitter[i].enabled = true;
            yield return new WaitForSeconds(stepTime);
            emitter[i].enabled = false;
        }
    }

}
