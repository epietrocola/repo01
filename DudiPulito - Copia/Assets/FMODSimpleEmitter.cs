﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODSimpleEmitter : MonoBehaviour {

    [FMODUnity.EventRef]
    public string FMODEvent;
    FMOD.Studio.EventInstance ev;

    // Use this for initialization
    void OnEnable () {
        FMOD.Studio.EventInstance ev = FMODUnity.RuntimeManager.CreateInstance(FMODEvent);
        ev.start();
    }

    void Update()
    {
        print(ev);
    }

    void OnDisable()
    {
        
    }

    public void StopEvent()
    {
        if (ev != null)
        {
            ev.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
    }
}
